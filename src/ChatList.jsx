import { Link, Outlet } from 'react-router-dom';

const users = ['kreshendo', 'umid', 'etibar', 'konul', 'zohre', 'gunay'];

function ChatList() {
  function load() {
    import('./ProstaSehife').then((module) => {
      console.log(module);
    });
  }

  return (
    <div className="row mt-5">
      <button onClick={load}>Load module</button>
      <ul className="col-3">
        {users.map((username) => (
          <li key={username}>
            <Link to={`/chat/${username}`}>{username}</Link>
          </li>
        ))}
      </ul>
      <div className="col-9">
        <Outlet />
      </div>
    </div>
  );
}

export default ChatList;
