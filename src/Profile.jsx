import { useParams, useNavigate, useLocation } from 'react-router-dom';

function Profile() {
  const { username } = useParams();
  const navigate = useNavigate();
  const location = useLocation();

  console.log(location);

  return (
    <>
      <h1>Hello from {username}'s Profile!</h1>
      <button onClick={() => navigate('/?page=3')}>Go back!</button>
    </>
  );
}

export default Profile;
