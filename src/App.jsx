import React, { Suspense } from 'react';
import { NavLink, Routes, Route } from 'react-router-dom';

const ChatContent = React.lazy(() => import('./ChatContent'));
const ChatList = React.lazy(() => import('./ChatList'));
const Index = React.lazy(() => import('./Index'));
const Profile = React.lazy(() => import('./Profile'));

function App() {
  return (
    <>
      <header>
        <nav className="container">
          <NavLink to="/">Index</NavLink>
          <NavLink to="/chat">Chat</NavLink>
        </nav>
      </header>
      <main className="container">
        <Suspense fallback={<h1>Loading...</h1>}>
          <Routes>
            <Route path="/" element={<Index />} />
            <Route path="/profile/:username" element={<Profile />} />
            <Route path="/chat" element={<ChatList />}>
              <Route path=":username" element={<ChatContent />} />
            </Route>
            <Route path="*" element={<h1>Not Found!</h1>} />
          </Routes>
        </Suspense>
      </main>
    </>
  );
}

export default App;
