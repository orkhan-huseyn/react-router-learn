import { useParams } from 'react-router-dom';

function ChatContent() {
  const { username } = useParams();

  return (
    <>
      <h1>Hello from {username}'s messages!</h1>
    </>
  );
}

export default ChatContent;
