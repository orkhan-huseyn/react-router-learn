import { Link, useSearchParams } from 'react-router-dom';
import Pagination from './Pagination';

const users = ['kreshendo', 'umid', 'etibar', 'konul', 'zohre', 'gunay'];

function Index() {
  const [searchParams, setSearchParams] = useSearchParams();

  const currentPage = searchParams.has('page') ? +searchParams.get('page') : 1;

  function handlePageChange(page) {
    setSearchParams({
      page,
    });
  }

  return (
    <>
      <ul>
        {users.map((username) => (
          <li key={username}>
            <Link to={`/profile/${username}`}>{username}</Link>
          </li>
        ))}
      </ul>
      <Pagination
        defaultCurrentPage={currentPage}
        onPageChange={handlePageChange}
        total={users.length}
        pageSize={2}
      />
    </>
  );
}

export default Index;
